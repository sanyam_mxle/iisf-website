import styles from "../styles/Landing.module.scss"
import SignUpButton from "../components/buttons/SignUpButton"
import Image from "next/image"
import SignInButton from "./buttons/SignInButton"

export default function Landing() {
    return (
        <div>
            <div className={styles.container}>
                <div className={styles.containerText}>
                    <h1>Are you suffering from PPD?</h1>
                    <h1>Care and Cure is our Goal.</h1>
                    <p>*Sign Up today and get 50% off on your first appointment</p>
                    <div className={styles.authButton}>
                        <SignUpButton />
                        <div className={styles.signIn}>
                            <SignInButton />
                        </div>
                    </div>
                </div>

                <div className={styles.containerImage}>
                    <Image src="/hospital.png" width={450} height={470} />
                </div>
            </div>
        </div>
    )
}