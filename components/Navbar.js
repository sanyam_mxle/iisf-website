import styles from "../styles/Navbar.module.scss"
import Link from "next/link"

export default function Navbar() {
    return (
        <div className={styles.navContainer}>
            <div className={styles.logo}>
                <p><a href="/">PPD Kit</a></p>
            </div>
            <div className={styles.navItems}>
                <Link href="/">About Us</Link>
                <Link href="/">Services</Link>
                <Link href="/">Contact Us</Link>
            </div>
        </div>
    )
}