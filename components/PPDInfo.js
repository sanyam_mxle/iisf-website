import styles from "../styles/PPDInfo.module.scss"
import Image from "next/image"

export default function PPDInfo() {
    return (
        <div className={styles.container}>
            <div className={styles.card}>
                <div className={styles.content}>
                    <h1>What is PPD?</h1>
                    <p>
                        Postpartum Depression is depression suffered by a mother following childbirth which begins within four week past delivery.
                    </p>
                    <br />
                    <p>
                        Studies have shown that 1 in 10 new fathers went through depression the same year when the child was born and can lead to other major depressions in life.
                    </p>
                    <br />
                    <p>
                        Postpartum Depression should not be confused as <span>"Baby Blues"</span> which is less severe and occurs for a short span of time of two weeks.
                    </p>
                </div>
                <div className={styles.image}>
                    <Image className={styles.image} src="/surgeon.png" width={250} height={250} />
                </div>
            </div>
            <div className={styles.card}>
                <div className={styles.content}>
                    <h1>Major Symptoms</h1>
                    <h1>& Causes</h1>
                    <ul>
                        <li>The major symptoms includes difficulty in bonding with the child, frequent mood swings and suicidal thoughts, loss of sleep, change in appetite and temperament.</li>
                        <li>A history of depression prior to becoming pregnant, or during pregnancy can add further to it.</li>
                        <li>The drop in the levels of female reproductive hormones estrogen and progesterone which increases tenfold during pregnancy and social and psychological changes of having a baby. These changes usually leads to PPD.</li>
                    </ul>
                </div>
                <div className={styles.image}>
                    <Image className={styles.image} src="/doctor.png" width={250} height={250} />
                </div>
            </div>
            <div className={styles.card}>
                <div className={styles.content}>
                    <h1>Natural Treatment and Remedies</h1>
                    <ul>
                        <li>Take balanced diet containing vitamins and minerals.</li>
                        <li>Take care of your body by regular exercise and proper sleep schedule.</li>
                        <li>Talk about it to either your family and friends or a recovered patient.</li>
                        <li>Avoid alcohol and recreational drugs.</li>
                    </ul>
                </div>
                <div className={styles.image}>
                    <Image className={styles.image} src="/doctor-female.png" width={250} height={250} />
                </div>
            </div>
        </div>
    )
}
