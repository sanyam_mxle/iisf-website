import styles from "../styles/Features.module.scss"
import { featureList } from "../features"

export default function Features() {
    return (
        <div className={styles.container}>
            <h1>What PPD Kit Offers</h1>
            <div className={styles.cardContainer}>
                {featureList.map(feature => (
                    <div className={styles.card} key={feature.key}>
                        <h2>{feature.title}</h2>
                        <p>{feature.content}</p>
                    </div>
                ))}
            </div>
        </div>
    )
}