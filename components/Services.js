import Image from "next/image"
import styles from "../styles/Services.module.scss"

export default function Services() {
    return (
        <div className={styles.container}>
            <div className={styles.textContainer}>
                <h1>Mood swings are real.</h1>
                <p><span>Myth</span>: Mood Swings are always an indication towards mental illness.</p>
                <br />
                <p><span>Truth</span>: Mood swings are completely normal and real. They are an unaccountable change of mood triggered by stress and part of dealing with a physical health condition. Albeit, extreme mood swings can be a characteristic of mental illness. </p>
            </div>

            <div className={styles.imageContainer}>
                <Image className={styles.image} src="/happy.png" width={200} height={200} />
                <Image className={styles.image}src="/worried.png" width={200} height={200} />
            </div>
        </div>
    )
}