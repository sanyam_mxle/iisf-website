import styles from "../../styles/buttons/SignUpButton.module.scss"

export default function SignUpButton() {
    return (
        <div>
            <button className={styles.signUp}>
                <a href="/register_patient">Sign Up</a>
            </button>
        </div>
    )
}