import styles from "../../styles/buttons/SignInButton.module.scss"

export default function SignInButton() {
    return (
        <div>
            <button className={styles.signIn}>
                <a href="/login">Sign In</a>
            </button>
        </div>
    )
}