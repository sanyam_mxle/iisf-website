import styles from "../styles/Footer.module.scss"
import Link from "next/link"

export default function Footer() {
    return (
        <div className={styles.container}>
            <div className={styles.logoContainer}>
                <div className={styles.logoContent}>
                    <h1><a href="/">PPD Kit</a></h1>
                    <p>a true well being</p>
                </div>
                <p className={styles.copyright}>*Copyright © 2021, PPD Kit. All rights reserved.</p>
            </div>

            <div className={styles.linkContainer}>
                <Link className={styles.link} href="/"><a>About Us</a></Link>
                <Link className={styles.link} href="/"><a>Services</a></Link>
                <Link className={styles.link} href="/"><a>Contact Us</a></Link>
            </div>
        </div>
    )
}
