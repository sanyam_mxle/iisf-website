import styles from "../styles/PreFooter.module.scss"
import SignUpButton from "./buttons/SignUpButton"

export default function PreFooter() {
    return (
        <div className={styles.container}>
            <h1>Book a consultation session Today!</h1>
            <div className={styles.authButton}>
                <SignUpButton />
            </div>
        </div>
    )
}
