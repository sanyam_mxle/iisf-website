export const featureList = [
    {
        key: "1",
        title: "News update regarding PPD",
        content: "News is always the best source of information that creates awareness among the people. Latest news related to Postpartum Depression will be available here."
    },
    {
        key: "2",
        title: "Awareness",
        content: "What is PPD? What are its symptoms? What are the causes leading to it? PPD and its diagnosis related information? We got you all covered!."
    },
    {
        key: "3",
        title: "Natural Treatment",
        content: "Can PPD Patient recover at home? How can you cure PPD at home? The natural treatment and remedies which will help in recovery."
    },
    {
        key: "4",
        title: "Self Assessment",
        content: "You can easily check whether you are suffering from PPD with our Self Assessment Bot. Find out about possible helps or assitance you might need."
    },
    {
        key: "5",
        title: "Drug Information and Guide",
        content: "Medication should be taken as prescribed with a proper schedule and quantity. The guide related to drugs for PPD is available."
    },
    {
        key: "6",
        title: "Consultation Session Booking",
        content: "Professional help is important when things get out of control. You can book a consultation session within few minutes just by a few clicks."
    }
]