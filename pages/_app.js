import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import { UserContext } from "../context/UserContext"
import { useState } from 'react'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [mobile, setMobile] = useState("");
  const [uid, setUid] = useState(null);
  // { username, setUsername, email, setEmail, password, setPassword, mobile, setMobile, uid, setUid }
  return (
    <UserContext.Provider value={{ username, setUsername, email, setEmail, password, setPassword, mobile, setMobile, uid, setUid }}>
      <Navbar />
      <Component {...pageProps} />
      <Footer />
    </UserContext.Provider>
  )
}

export default MyApp