import Features from "../components/Features"
import Footer from "../components/Footer"
import Landing from "../components/Landing"
import PPDInfo from "../components/PPDInfo"
import PreFooter from "../components/PreFooter"
import Services from "../components/Services"
import styles from "../styles/Index.module.scss"

export default function Home() {

  return (
    <div className={styles.mainContainer}>
      <Landing />
      <hr className={styles.hrLine} />
      <PPDInfo />
      <hr className={styles.hrLine} />
      <Features />
      <hr className={styles.hrLine} />
      <Services />
      <PreFooter />
      {/* <Footer /> */}
    </div>
  )
}