// separate sign up for doctor & patient
import Image from "next/image"
import { firebase } from "../lib/firebase"
import { GoogleAuthProvider } from "firebase/auth";
import { useState, useContext } from "react";
import "firebase/auth"
import styles from "../styles/Register.module.scss"
import toast from "react-hot-toast"
import 'firebase/firestore';
import { UserContext } from "../context/UserContext";

export default function signUp() {
    const { username, setUsername } = useContext(UserContext);
    const { email, setEmail } = useContext(UserContext);
    const { password, setPassword } = useContext(UserContext);
    const { mobile, setMobile } = useContext(UserContext);
    const { uid, setUid } = useContext(UserContext);

    // state management for signed in user
    const [isUserSignedIn, setIsUserSignedIn] = useState(true);

    // database initialization
    const db = firebase.firestore();

    // storing and using form data for authentication
    async function handleSubmit(e) {
        e.preventDefault();
        const type = 'patient';
        await firebase.auth().createUserWithEmailAndPassword(email, password).then(cred => {
            // window.location.href = "/dashboard";
            console.log(cred);
            setUid(cred.user.uid);
            return db.collection("signup-details").doc(cred.user.uid).set({  // saving authenticated uid to firestore document uid for mutual link
                name: username,
                email: email,
                password: password,
                mobile: mobile,
                type: type
            })
        }).catch(function (err) {
            //error handle
            const msg = err.message;
            toast.error(msg);
        })
        console.log("Successfully Signed Up");
    }


    // check if user exits and set 'setIsUserSignedIn' to true
    firebase.auth().onAuthStateChanged((user) => {
        // getting data from firestore
        if (user) {
            setIsUserSignedIn(true);
        } else {
            setIsUserSignedIn(false);
        }
    })

    // signs out the user
    const signOut = () => {
        firebase.auth().signOut().then(() => {
            console.log("Signed Out");
        }).catch((err) => {
            console.log(err);
        });
    }

    // if user is signed in, then display 'Signed in and Sign out button' 
    // else display sign up form
    if (isUserSignedIn) {
        return (
            <div className={styles.container}>
                <div className={styles.image}>
                    <Image src="/diet.png" width={320} height={300} />
                </div>
                <div className={styles.card}>
                    <h1>Signed In</h1>
                    <button onClick={signOut}>
                        Sign Out
                    </button>
                    <h2>Go to your Dashboard</h2>
                    <a href="/dashboard">here</a>
                </div>
            </div>
        )
    } else {
        return (
            <div className={styles.container}>
                <div className={styles.image}>
                    <Image src="/diet.png" width={320} height={300} />
                </div>
                <div className={styles.formContainer}>
                    <div>
                        <h1>Sign Up as Patient</h1>
                        <p>Are you a doctor? Sign up <a href="/register_doctor">here</a></p>
                        <p><a href="/login">Log In</a> instead?</p>
                    </div>
                    <form onSubmit={handleSubmit} autoComplete="off">
                        <input type="text" placeholder="Email" id="email" required onChange={(e) => {
                            setEmail(e.target.value)
                            console.log(e.target.value);
                        }} />
                        <input type="text" placeholder="Name" id="name" required onChange={(e) => setUsername(e.target.value)} />
                        <input type="password" placeholder="Password" id="password" required onChange={(e) => setPassword(e.target.value)} />
                        <input type="text" placeholder="Mobile Number" id="mobile" required onChange={(e) => setMobile(e.target.value)} />
                        <button>Sign Up</button>
                    </form>
                </div>
            </div>
        )
    }
}