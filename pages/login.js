// separate sign up for doctor & patient
import Image from "next/image"
import { firebase } from "../lib/firebase"
import "firebase/firestore"
import { GoogleAuthProvider } from "firebase/auth";
import { useState } from "react";
import styles from "../styles/Register.module.scss"
import toast from "react-hot-toast"

export default function signUp() {
    // state management for signed in user
    const [isUserSignedIn, setIsUserSignedIn] = useState(true);

    // storing and using form data for authentication
    async function handleSubmit(e) {
        e.preventDefault();
        const { email, password } = e.target.elements;
        await firebase.auth().signInWithEmailAndPassword(email.value, password.value)
            .then((userCredentials) => {
                // console.log(userCredentials.user.uid);
            })
            
        window.location.href = "/dashboard";
        console.log("Successfully Signed In");
    }

    // check if user exits and set 'setIsUserSignedIn' to true
    firebase.auth().onAuthStateChanged((user) => {
        // getting data from firestore
        if (user) {

            console.log(user.uid);
            setIsUserSignedIn(true);
        } else {
            setIsUserSignedIn(false);
        }
    })

    // signs out the user
    const signOut = () => {
        firebase.auth().signOut().then(() => {
            console.log("Signed Out");
        }).catch((err) => {
            console.log(err);
        });
    }

    // if user is signed in, then display 'Signed in and Sign out button' 
    // else display sign up form
    if (isUserSignedIn) {
        return (
            <div className={styles.container}>
                <div className={styles.image}>
                    <Image src="/diet.png" width={320} height={300} />
                </div>
                <div className={styles.card}>
                    <h1>Signed In</h1>
                    <button onClick={signOut}>
                        Sign Out
                    </button>
                    <h2>Go to your Dashboard</h2>
                    <a href={`/dashboard`}>here</a>
                </div>
            </div>
        )
    } else {
        return (
            <div className={styles.container}>
                <div className={styles.image}>
                    <Image src="/diet.png" width={320} height={300} />
                </div>
                <div className={styles.formContainer}>
                    <div>
                        <h1>Log In</h1>
                        <p><a href="/register_patient">Sign Up</a> instead?</p>
                    </div>
                    <form onSubmit={handleSubmit} autoComplete="off">
                        <input type="text" placeholder="Email" id="email" required />
                        <input type="password" placeholder="Password" id="password" required />
                        <button>Sign In</button>
                    </form>
                </div>
            </div>
        )
    }
}