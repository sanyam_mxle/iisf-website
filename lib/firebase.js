import firebase from "firebase/app"

const firebaseConfig = {
    apiKey: "AIzaSyAG3U3CEvKDgkdoUw_fO3EEW882sbZ1kbQ",
    authDomain: "ppd-kit.firebaseapp.com",
    projectId: "ppd-kit",
    storageBucket: "ppd-kit.appspot.com",
    messagingSenderId: "429961192875",
    appId: "1:429961192875:web:b9f8d99fbce35563a2deee"
};


if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };